﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: GpioPinExtensions.cs Last modified: 2015-11-14@04:14 by Tim Long

using System.Diagnostics.Contracts;
using Windows.Devices.Gpio;

namespace TA.Iot
{
    internal static class GpioPinExtensions
    {
        public static void Write(this GpioPin pin, bool value)
        {
            Contract.Requires(pin != null);
            pin.Write(value ? GpioPinValue.High : GpioPinValue.Low);
        }
    }
}