﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2016 Tigra Astronomy, all rights reserved.
// 
// File: TaskExtensions.cs Last modified: 2016-01-05@13:14 by Tim Long

using System.Threading.Tasks;

namespace TA.Iot
{
    public static class TaskExtensions
        {
        public static TResult WaitFoResult<TResult>(this Task<TResult> asyncOperation)
            {
            asyncOperation.Wait();
            return asyncOperation.Result;
            }
        }
    }