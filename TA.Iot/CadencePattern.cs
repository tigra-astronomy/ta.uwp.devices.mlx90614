// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: CadencePattern.cs Last modified: 2015-11-14@04:14 by Tim Long

namespace TA.Iot
{
    /// <summary>
    ///     Pre-defined cadence patterns for <see cref="CadencedOutputPin" />.
    /// </summary>
    public static class CadencePattern
    {
        public const ulong SteadyOn = 0xFFFFFFFFFFFFFFFF;
        public const ulong SteadyOff = 0x0;
        public const ulong BlinkSlow = 0xFFFFFFFF00000000;
        public const ulong BlinkMedium = 0xFFFF0000FFFF0000;
        public const ulong BlinkFast = 0xF0F0F0F0F0F0F0F0;
        public const ulong Alarm = 0x8000800080008000;
        public const ulong Strobe = 0xAAAAAAAAAAAAAAAA;
        public const ulong Wink = 0xFFFFFFFFFFFFFFFC;
        public const ulong ActivityIndicator = 0x8000000000000000;
        public const ulong SOS = 0xCCC03C3C3C0CCC00;
        public const ulong SMS = 0xCCC03C3C0CCC0000;
    }
}