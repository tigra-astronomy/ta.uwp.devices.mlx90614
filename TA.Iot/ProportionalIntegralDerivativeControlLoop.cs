﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2017 Tigra Astronomy, all rights reserved.
// 
// File: ProportionalIntegralDerivativeControlLoop.cs Last modified: 2017-01-31@23:02 by Tim Long

using System;
using System.Diagnostics.Contracts;
using System.Threading;
using TA.UWP.ExtensionMethods;

namespace TA.Iot
    {
    /// <summary>
    ///     This class draws inspiration from
    ///     http://www.codeproject.com/Articles/49548/Industrial-NET-PID-Controllers. However, we
    ///     wrote our own version from scratch, using the information presented by Ron Beyer in his
    ///     CodeProject aticle.  We did this for a few reasons: We didn't want to be bound by the
    ///     CodeProject CPOL license; We felt that we'd understand the code better if we had written
    ///     it ourselves. We had much of the math code already implemented as extension methods and
    ///     we didn't want duplicate code; Threads are too expensive to have one that is blocked
    ///     most of the time. We used a timer instead; We wanted the PID loop frequency to be
    ///     configurable; We wanted to use test-first development and code contracts.
    /// </summary>
    /// <image url="$(ProjectDir)\images\pid.jpg" scale="0.75" />
    public class ProportionalIntegralDerivativeControlLoop
        {
        private const double ErrorLimit = 100.0; // Arbitrary limit on the accumulated error.
        private readonly TimeSpan computePeriod;
        private double lastProcessVariable;
        private DateTime lastUpdateTime = DateTime.UtcNow;
        private Timer pidLoopTimer;
        private Func<double> readProcessVariable;
        private Func<double> readSetPoint;
        private Action<double> writeOutputValue;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProportionalIntegralDerivativeControlLoop" /> class.
        /// </summary>
        /// <param name="proportionalGain">The proportional gain.</param>
        /// <param name="integralGain">The integral gain.</param>
        /// <param name="derivativeGain">The derivative gain.</param>
        /// <param name="processVariableMaximumValue">The process variable maximum value.</param>
        /// <param name="processVariableMinimumValue">The process variable minimum value.</param>
        /// <param name="outputMaximumValue">The output maximum value.</param>
        /// <param name="outputMinimumValue">The output minimum value.</param>
        /// <param name="readProcessVariable">A delegate that returns the value of the Process Variable.</param>
        /// <param name="readSetPoint">A delegate that returns the value of the SetPoint.</param>
        /// <param name="writeOutputValue">A method that accepts the new output value.</param>
        /// <param name="frequency">The frequency (times per second) that the output value is computed. Default is 10Hz.</param>
        public ProportionalIntegralDerivativeControlLoop(
            double proportionalGain, double integralGain, double derivativeGain, double processVariableMaximumValue,
            double processVariableMinimumValue, double outputMaximumValue, double outputMinimumValue,
            Func<double> readProcessVariable, Func<double> readSetPoint, Action<double> writeOutputValue,
            double frequency = 1.0)
            {
            Contract.Requires(processVariableMaximumValue > processVariableMinimumValue);
            Contract.Requires(outputMaximumValue > outputMinimumValue);
            Contract.Requires(readProcessVariable != null);
            Contract.Requires(readSetPoint != null);
            Contract.Requires(writeOutputValue != null);
            Contract.Requires(frequency > 0 && frequency <= 1000);
            ProportionalGain = proportionalGain;
            IntegralGain = integralGain;
            DerivativeGain = derivativeGain;
            ProcessVariableMaximumValue = processVariableMaximumValue;
            ProcessVariableMinimumValue = processVariableMinimumValue;
            OutputMaximumValue = outputMaximumValue;
            OutputMinimumValue = outputMinimumValue;
            this.readProcessVariable = readProcessVariable;
            this.readSetPoint = readSetPoint;
            this.writeOutputValue = writeOutputValue;
            computePeriod = TimeSpan.FromSeconds(1.0 / frequency);
            }

        public double ProportionalGain { get; set; }

        public double IntegralGain { get; set; }

        public double DerivativeGain { get; set; }

        public double ProcessVariableMinimumValue { get; set; }

        public double ProcessVariableMaximumValue { get; set; }

        public double OutputMinimumValue { get; set; }

        public double OutputMaximumValue { get; set; }

        public double ProportionalTerm { get; private set; }

        public double IntegralTerm { get; private set; }

        public double DerivativeTerm { get; private set; }

        public double CumulativeError { get; private set; }

        public double InstantaneousError { get; private set; }

        public bool PidRunning
            => pidLoopTimer != null && readProcessVariable != null && readSetPoint != null && writeOutputValue != null;

        /// <summary>
        ///     Finalizes an instance of the <see cref="ProportionalIntegralDerivativeControlLoop" /> class.
        /// </summary>
        ~ProportionalIntegralDerivativeControlLoop()
            {
            Stop();
            readProcessVariable = null;
            readSetPoint = null;
            writeOutputValue = null;
            }

        /// <summary>
        ///     Starts or restarts the PID loop.
        /// </summary>
        public void Start()
            {
            Stop();
            Reset();
            pidLoopTimer = new Timer(ComputeOutputValue, null, computePeriod, computePeriod);
            }

        /// <summary>
        ///     Stops the PID loop.
        /// </summary>
        public void Stop()
            {
            if (pidLoopTimer == null)
                return;

            pidLoopTimer.Dispose();
            pidLoopTimer = null;
            }

        /// <summary>
        ///     Resets the PID loop.
        /// </summary>
        public void Reset()
            {
            CumulativeError = 0.0f;
            lastUpdateTime = DateTime.UtcNow;
            }

        private void ComputeOutputValue(object ignored)
            {
            if (readProcessVariable == null || readSetPoint == null || writeOutputValue == null)
                return;
            var unclippedProcessVariable = readProcessVariable();
            var processVariable =
                unclippedProcessVariable.Clip(ProcessVariableMinimumValue, ProcessVariableMaximumValue)
                    .MapToRange(ProcessVariableMinimumValue, ProcessVariableMaximumValue, -1.0, 1.0);
            var setpoint =
                readSetPoint()
                    .Clip(ProcessVariableMinimumValue, ProcessVariableMaximumValue)
                    .MapToRange(ProcessVariableMinimumValue, ProcessVariableMaximumValue, -1.0, 1.0);
            var delta = setpoint - processVariable; // error in percent
            var proportionalTerm = delta * ProportionalGain;
            var integralTerm = 0.0;
            var derivativeTerm = 0.0;
            var now = DateTime.UtcNow;
            var deltaTime = (now - lastUpdateTime).TotalSeconds;

            // Determine the cumulative error that would result in the I term being >= ±100% and clip the value there.
            // Care is needed to avoid divide-by-zero when the gain is 0. This prevents integral wind-up.
            var errorLimit = Math.Abs(IntegralGain) > double.Epsilon ? 1.0 / IntegralGain : 1.0;
            CumulativeError = (CumulativeError + deltaTime * delta).Clip(-errorLimit, +errorLimit);
            integralTerm = IntegralGain * CumulativeError;
            // Compute the derivative term only if dT is non-zero. Avoid possible problems with floating point comparison.
            if (Math.Abs(deltaTime) > 0)
                {
                var deltaProcessVariable = processVariable - lastProcessVariable;
                derivativeTerm = DerivativeGain * deltaProcessVariable / deltaTime;
                }
            // Now we have all the terms; they are summed and mapped to the output range; then written to the external system.
            var sumOfTerms = proportionalTerm + integralTerm + derivativeTerm;
            var outputValue = sumOfTerms.Clip(-1.0, +1.0).MapToRange(-1.0, +1.0, OutputMinimumValue, OutputMaximumValue);
            // Set properties that expose the internal workings of the PID loop
            ProportionalTerm = proportionalTerm;
            IntegralTerm = integralTerm;
            DerivativeTerm = derivativeTerm;
            InstantaneousError = delta;
            // Finally, write the new output value and update internal state for next iteration.
            writeOutputValue(outputValue);
            lastUpdateTime = now;
            lastProcessVariable = processVariable;
            }
        }
    }