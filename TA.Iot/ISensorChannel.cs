﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2016 Tigra Astronomy, all rights reserved.
// 
// File: ISensorChannel.cs Last modified: 2016-12-24@06:10 by Tim Long

using System.Threading.Tasks;

namespace TA.Iot
    {
    /// <summary>
    ///     Sensor values are readings obtained from some hardware device. Conceptually, sensor
    ///     values are normalized to a fraction of unity, i.e. are in the range 0.0 to 1.0
    ///     inclusive. A sensor channel provides a means of accessing the most recently obtained
    ///     value and a way of refreshing the value with new data from the sensor, if appropriate.
    /// </summary>
    public interface ISensorChannel
        {
        /// <summary>
        ///     Gets the most recently obtained sensor value, in scaled world units.
        /// </summary>
        /// <value>The value.</value>
        double Value { get; }

        /// <summary>
        ///     Gets or sets the sensor scale, which can be used to scale raw sensor readings into world units.
        ///     Unscaled values are in the range 0.0 to 1.0.
        /// </summary>
        /// <value>The scale.</value>
        double Scale { get; set; }

        /// <summary>
        ///     Gets the precision of the sensor. Precision is the smallest amount by which the value can change
        ///     and is in the range 0.0 to 1.0. The precision is typically inherent to the design of the sensor
        ///     and so is not settable.
        /// </summary>
        /// <value>The precision.</value>
        double Precision { get; }

        /// <summary>
        ///     Returns a task which, when complete, results in a fresh reading being obtained from
        ///     the sensor. This will often be an IO bound operation and is a natural fit for an
        ///     asynchronous method.
        /// </summary>
        /// <remarks>
        ///     Note: some sensors cannot produce readings on demand
        /// </remarks>
        Task UpdateValue();
        }
    }