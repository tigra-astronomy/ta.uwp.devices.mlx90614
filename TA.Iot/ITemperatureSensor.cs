// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: ITemperatureSensor.cs Last modified: 2015-11-14@04:14 by Tim Long

using System;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using TA.UWP;

namespace TA.Iot
{
    [ContractClass(typeof(TemperatureSensorContract))]
    public interface ITemperatureSensor
    {
        /// <summary>
        ///     Gets the value of the specified A-to-D channel.
        /// </summary>
        /// <param name="channel">The channel to be read.</param>
        /// <returns>
        ///     The scaled value of the channel.
        /// </returns>
        double this[uint channel] { get; }

        /// <summary>
        ///     Gets or sets the scale. Defaults to 1.0 for readings directly in Kelvin.
        /// </summary>
        /// <value>
        ///     The scale - samples are multiplied by the scale as they are obtained so that the moving average
        ///     values are also scaled. The default scale for a newly created instance is 1.0
        /// </value>
        double Scale { get; set; }

        /// <summary>
        ///     Resets and reinitializes any hardware devices and clears all readings to zero.
        /// </summary>
        void Reset();

        /// <summary>
        ///     sample all channels as an asynchronous operation.
        /// </summary>
        Task SampleAllChannelsAsync();
    }

    [ContractClassFor(typeof(ITemperatureSensor))]
    internal abstract class TemperatureSensorContract : ITemperatureSensor
    {
        [ContractInvariantMethod]
        private void ObjectInvariant() { Contract.Invariant(Scale != 0); }

        #region Implementation of ITemperatureSensor

        public double this[uint channel] { get { throw new NotImplementedException(); } }

        public double Scale { get; set; }

        public void Reset() { throw new NotImplementedException(); }

        public Task SampleAllChannelsAsync() { throw new NotImplementedException(); }

        public void StartPeriodicSampling(Timeout interval) { Contract.Requires(interval.Milliseconds >= 1); }

        public void StopPeriodicSampling() { }

        #endregion
    }
}