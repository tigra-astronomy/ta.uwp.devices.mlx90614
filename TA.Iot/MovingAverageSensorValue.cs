﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2016 Tigra Astronomy, all rights reserved.
// 
// File: MovingAverageSensorValue.cs Last modified: 2016-12-19@16:35 by Tim Long

using System;
using System.Diagnostics.Contracts;

namespace TA.Iot
    {
    /// <summary>
    ///     Uses the decorator pattern to add a moving average to any type that implements
    ///     <see cref="INotifySensorValueChanged" />.
    /// </summary>
    /// <seealso cref="TA.UWP.IoTUtilities.INotifySensorValueChanged" />
    public class MovingAverageSensorValue : INotifySensorValueChanged
        {
        private readonly MovingAverage sensorValue;
        private readonly INotifySensorValueChanged source;


        /// <summary>
        ///     Initializes a new instance of the <see cref="MovingAverageSensorValue" /> class.
        /// </summary>
        /// <param name="source">The underlying data source.</param>
        /// <param name="movingAverageWindowSize">
        ///     The number of samples in the moving average window.
        /// </param>
        public MovingAverageSensorValue(INotifySensorValueChanged source, int movingAverageWindowSize)
            {
            Contract.Requires(movingAverageWindowSize > 1);
            this.source = source;
            sensorValue = new MovingAverage(movingAverageWindowSize);
            source.ValueChangedThreshold = 0;
            source.ValueChanged += HandleSourceValueChanged;
            }

        /// <summary>
        ///     Gets or sets the amount that a value must change by in order to trigger the
        ///     <see cref="E:INotifySensorValueChanged.ValueChanged" />event.  The default value for
        ///     this property is implementation specific and will typically be related to the precision
        ///     of the sensor.
        /// </summary>
        /// <value>The threshold.</value>
        public double ValueChangedThreshold
            {
            get { return source.ValueChangedThreshold; }
            set { source.ValueChangedThreshold = value; }
            }

        public event EventHandler<SensorValueChangedEventArgs> ValueChanged;

        private void HandleSourceValueChanged(object sender, SensorValueChangedEventArgs e)
            {
            var referenceValue = sensorValue.Average;
            sensorValue.AddSample(e.Value);
            var updatedAverage = sensorValue.Average;
            var delta = updatedAverage - referenceValue;
            if (Math.Abs(delta) >= ValueChangedThreshold)
                {
                var args = new SensorValueChangedEventArgs(e.Channel, updatedAverage);
                RaiseValueChanged(args);
                }
            }

        protected void RaiseValueChanged(SensorValueChangedEventArgs args)
            {
            ValueChanged?.Invoke(this, args);
            }
        }
    }