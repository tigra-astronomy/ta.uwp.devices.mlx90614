// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2016 Tigra Astronomy, all rights reserved.
// 
// File: MovingAverage.cs Last modified: 2016-12-25@19:22 by Tim Long

using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace TA.Iot
    {
    /// <summary>
    ///     Provides a moving average over a number of samples.
    /// </summary>
    public class MovingAverage
        {
        private readonly Queue<double> sampleQueue;
        private readonly object sync = new object();
        protected int index;
        private double runningTotal;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MovingAverage" /> class and sets the sample population size.
        /// </summary>
        /// <param name="windowSize">The number of samples in the moving average window.</param>
        public MovingAverage(int windowSize)
            {
            WindowSize = windowSize;
            Contract.Requires(windowSize > 1, "Population size cannot be less than 2");
            sampleQueue = new Queue<double>(windowSize);
            index = 0;
            Clear();
            }

        public double Average => sampleQueue.Count == 0 ? 0 : runningTotal / sampleQueue.Count;

        public int WindowSize { get; }

        /// <summary>
        ///     Adds a sample to the moving average.
        /// </summary>
        /// <param name="value">The value to be added.</param>
        public void AddSample(double value)
            {
            lock (sync)
                {
                var oldestSample = sampleQueue.Count >= WindowSize ? sampleQueue.Dequeue() : 0.0;
                runningTotal = runningTotal - oldestSample + value;
                sampleQueue.Enqueue(value);
                }
            }

        /// <summary>
        ///     Adds a sample to the moving average.
        /// </summary>
        /// <param name="value">The value to be added.</param>
        public void AddSample(int value)
            {
            AddSample((double) value);
            }

        /// <summary>
        ///     Clears all the samples, either to 0 or the specified value.
        /// </summary>
        /// <param name="initialValue">
        ///     The initial value to set all the samples to, and therefore the moving average value. This can be useful when
        ///     you would like the moving average to start from a value other than zero.
        /// </param>
        public void Clear(double initialValue = default(double))
            {
            lock (sync)
                {
                sampleQueue.Clear();
                runningTotal = 0;
                if (initialValue != default(double))
                    AddSample(initialValue);
                }
            }
        }
    }