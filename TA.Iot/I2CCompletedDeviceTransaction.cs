﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: I2CCompletedDeviceTransaction.cs Last modified: 2015-11-14@04:14 by Tim Long

using Windows.Devices.I2c;

namespace TA.Iot
{
    /// <summary>
    ///     Represents a completed transaction with an I2C device.
    /// </summary>
    public sealed class I2CCompletedDeviceTransaction : DeviceTransaction
    {
        internal I2CCompletedDeviceTransaction(
            IDeviceTransaction source, II2CDevice device, I2cTransferResult result,
            I2CPecStatus check = I2CPecStatus.NotChecked) : base(source.TransmitBuffer, source.ReceiveBuffer)
        {
            Device = device;
            Result = result;
            PecStatus = check;
        }

        /// <summary>
        ///     Gets the device with which the transaction was conducted.
        /// </summary>
        /// <value>The device.</value>
        public II2CDevice Device { get; }

        /// <summary>
        ///     Gets the result of the transaction.
        /// </summary>
        /// <value>The result.</value>
        public I2cTransferResult Result { get; }

        /// <summary>
        ///     Gets the Packet Error Check (CRC) status.
        /// </summary>
        /// <value>The pec status.</value>
        public I2CPecStatus PecStatus { get; }
    }
}