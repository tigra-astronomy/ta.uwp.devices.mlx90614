﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2016 Tigra Astronomy, all rights reserved.
// 
// File: SimpleTransactionBuilder.cs Last modified: 2016-01-13@20:30 by Tim Long

using System.Collections.Generic;
using System.Linq;

namespace TA.Iot
{
    /// <summary>
    ///     A builder pattern for constructing an <see cref="IDeviceTransaction" />. This trivial implementation will most
    ///     likely serve as a base for more device-specific implementations.
    /// </summary>
    public class SimpleTransactionBuilder : ITransactionBuilder
        {
        readonly List<byte> transmitBuffer = new List<byte>();
        int expectedResponseSize;

        #region Implementation of ITransactionBuilder

        /// <summary>
        ///     Appends one or more bytes to the transmit buffer.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns>ITransactionBuilder.</returns>
        public ITransactionBuilder Append(params byte[] bytes)
            {
            transmitBuffer.AddRange(bytes);
            return this;
            }

        /// <summary>
        ///     Appends one or more unsigned integers to the transmit buffer. The integers are truncated to octets before
        ///     appending.
        /// </summary>
        /// <param name="items">Any number of unsigned integers.</param>
        /// <returns>ITransactionBuilder.</returns>
        protected ITransactionBuilder Append(params uint[] items)
            {
            foreach (var item in items)
                Append((byte) item);
            return this;
            }

        /// <summary>
        ///     Appends one or more integers to the transmit buffer. The integers are converted to bytes by truncating them to the
        ///     least significant 8 bits.
        /// </summary>
        /// <param name="items">The items.</param>
        protected ITransactionBuilder Append(params int[] items)
            {
            foreach (var item in items)
                Append((byte) item);
            return this;
            }


        /// <summary>
        /// Computes a checksum of the transmit buffer using the supplied algorithm and appends the checksum bytes to the transmit buffer.
        /// </summary>
        /// <param name="algorithm">The algorithm usd to compute the checksum value.</param>
        /// <param name="header">Header bytes that are not contained in the transmit buffer but which must be included in the checksum computation.</param>
        /// <returns>ITransactionBuilder.</returns>
        public ITransactionBuilder AppendChecksum(IComputeChecsum algorithm, byte[] header)
        {
            var preamble = header ?? new byte[0];
            IEnumerable<byte> packet = preamble.Concat(transmitBuffer);
            var crc = algorithm.Checksum(packet.ToArray<byte>());
            return Append(crc);
        }


        public ITransactionBuilder ExpectResponse(int bytes)
            {
            expectedResponseSize += bytes;
            return this;
            }

        public IDeviceTransaction Build()
            {
            return new DeviceTransaction(transmitBuffer.ToArray(), new byte[expectedResponseSize]);
            }

        #endregion
        }
    }