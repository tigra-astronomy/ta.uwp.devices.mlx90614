// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2016 Tigra Astronomy, all rights reserved.
// 
// File: INotifySensorValueChanged.cs Last modified: 2016-12-19@16:42 by Tim Long

using System;

namespace TA.Iot
    {
    /// <summary>
    ///     Implementers have the ability to notify subscribers (via the <see cref="ValueChanged" />
    ///     event) when a sensor value has changed by a configurable amount, specified by the
    ///     <see cref="ValueChangedThreshold" />property.
    /// </summary>
    /// <remarks>
    ///     Implementers of this interface often also implement <see cref="IPeriodicSampling" />,
    ///     but it should not be assumed that this is the case. This interface only defines the
    ///     behaviour of being able to provide event based notifications and does not govern how
    ///     those notifications are triggered. In some cases, it may be necessary to manually
    ///     trigger a sensor reading while in other cases, the device may take periodic readings.
    /// </remarks>
    /// <seealso cref="IPeriodicSampling" />
    public interface INotifySensorValueChanged
        {
        /// <summary>
        ///     Gets or sets the amount that a value must change by in order to trigger the
        ///     <see cref="ValueChanged" /> event. The default value for this property is
        ///     implementation specific and will typically be related to the precision of the
        ///     underlying sensor.
        /// </summary>
        /// <value>
        ///     The threshold for generating a <see cref="ValueChanged" /> notification.  Caution:
        ///     setting this to zero will result in notifications being generated for each and every
        ///     new datum, regardless of whether it differs from the previous value. This could
        ///     generate a lot of events.
        /// </value>
        double ValueChangedThreshold { get; set; }

        /// <summary>
        ///     Occurs when a sensor channel's value changes by an amount greater then or equal to
        ///     <see cref="ValueChangedThreshold" />. You should not assume that events will be
        ///     raised on any particular thread.
        /// </summary>
        event EventHandler<SensorValueChangedEventArgs> ValueChanged;
        }
    }