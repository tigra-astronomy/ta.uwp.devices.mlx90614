// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: IDeviceTransaction.cs Last modified: 2015-11-14@04:14 by Tim Long

using System.Diagnostics.Contracts;

namespace TA.Iot
{
    /// <summary>
    ///     Data to be transmitted and corresponding data received from a device.
    /// </summary>
    [ContractClass(typeof(DeviceTransactionContract))]
    public interface IDeviceTransaction
    {
        /// <summary>
        ///     The byte stream to be transmitted, if any.
        /// </summary>
        /// <value>The write transaction.</value>
        byte[] TransmitBuffer { get; }

        /// <summary>
        ///     The receive buffer which, if present, will be of the correct size to hold the expected response.
        /// </summary>
        /// <value>The read transaction.</value>
        byte[] ReceiveBuffer { get; }
    }

    [ContractClassFor(typeof(IDeviceTransaction))]
    internal abstract class DeviceTransactionContract : IDeviceTransaction
    {
        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            // The buffers may be empty, but not null.
            Contract.Invariant(TransmitBuffer != null);
            Contract.Invariant(ReceiveBuffer != null);
        }

        #region Implementation of IDeviceTransaction

        /// <summary>
        ///     The byte stream to be transmitted, if any.
        /// </summary>
        /// <value>The write transaction.</value>
        public abstract byte[] TransmitBuffer { get; }

        /// <summary>
        ///     The receive buffer which, if present, will be of the correct size to hold the expected response.
        /// </summary>
        /// <value>The read transaction.</value>
        public abstract byte[] ReceiveBuffer { get; }

        #endregion
    }
}