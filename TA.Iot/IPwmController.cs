﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2016 Tigra Astronomy, all rights reserved.
// 
// File: IPwmController.cs Last modified: 2016-06-04@16:02 by Tim Long

using System;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using TA.UWP.ExtensionMethods;

namespace TA.Iot
    {
    [ContractClass(typeof(IPwmControllerContract))]
    public interface IPwmController
        {
        /// <summary>
        ///     Gets the actual frequency of the PWM output, which may differ from that requested in
        ///     <see cref="SetDesiredFrequency" />.
        /// </summary>
        /// <value>The actual frequency, in cycles per second (Hertz).</value>
        double ActualFrequency { get; }

        /// <summary>
        ///     Gets the maximum frequency that the controller is able to support.
        /// </summary>
        /// <value>The maximum frequency in cycles per second.</value>
        double MaxFrequency { get; }

        /// <summary>
        ///     Gets the minimum frequency that the controller is able to support. This is not necessarily zero.
        /// </summary>
        /// <value>The minimum frequency, in cycles per second.</value>
        double MinFrequency { get; }

        /// <summary>
        ///     Gets the number of PWM channels available on this controller. Channel numbers start at 0.
        /// </summary>
        /// <value>The zero-based channel count.</value>
        int ChannelCount { get; }

        /// <summary>
        ///     Gets the <see cref="IPwmChannel" /> with the specified channel index.
        /// </summary>
        /// <param name="channelIndex">Index of the channel.</param>
        /// <returns>IPwmChannel.</returns>
        IPwmChannel this[uint channelIndex] { get; }

        /// <summary>
        ///     Sets the desired frequency for the PWM controller, in cycles per second. The channel output will perform
        ///     one full cycle at this frequency. The actual frequency used will be the closest frequency that the
        ///     actual device is capable of and will be available in the <see cref="ActualFrequency" /> property.
        /// </summary>
        /// <param name="desiredFrequency">
        ///     The desired PWM output modulation frequency, which must be in the range
        ///     <see cref="MinFrequency" /> to <see cref="MaxFrequency" /> . If a value outside that range is given,
        ///     then the behaviour is undefined; the value may simply be clipped or an exception might result. Setting
        ///     the frequency may render other device configuration invalid and channels may need to be reconfigured. It
        ///     is recommended that setting the frequency is done once when the controller instance is created.
        /// </param>
        Task SetDesiredFrequency(double desiredFrequency);

        /// <summary>
        ///     Sets all channels to 0% duty cycle.
        /// </summary>
        Task SetAllChannelsOff();

        /// <summary>
        ///     Resets the controller to a known ready state so that the device is ready for use.
        ///     If possible, all channels should be set to 0% duty cycle and the PWM frequency should be set to the default for the
        ///     device.
        /// </summary>
        Task Reset();
        }

    [ContractClassFor(typeof(IPwmController))]
    internal abstract class IPwmControllerContract : IPwmController
        {
        [ContractInvariantMethod]
        private void ObjectInvariant()
            {
            Contract.Invariant(MaxFrequency >= MinFrequency);
            Contract.Invariant(MinFrequency > 0);
            Contract.Invariant(ChannelCount > 0);
            Contract.Invariant(ActualFrequency >= MinFrequency && ActualFrequency <= MaxFrequency);
            Contract.Invariant(ChannelCount > 0);
            }

        #region Implementation of IPwmController
        public Task SetDesiredFrequency(double desiredFrequency)
            {
            Contract.Requires(desiredFrequency.IsInRange(MinFrequency, MaxFrequency));
            Contract.Ensures(ActualFrequency.IsInRange(MinFrequency, MaxFrequency));
            throw new NotImplementedException();
            }

        public double ActualFrequency
            {
            get
                {
                Contract.Ensures(Contract.Result<double>() >= MinFrequency);
                Contract.Ensures(Contract.Result<double>() <= MaxFrequency);
                throw new NotImplementedException();
                }
            }

        public double MaxFrequency { get; }

        public double MinFrequency { get; }

        public int ChannelCount { get; }

        /// <summary>
        ///     Gets the <see cref="IPwmChannel" /> with the specified channel index.
        /// </summary>
        /// <param name="channelIndex">Index of the channel.</param>
        /// <returns>IPwmChannel.</returns>
        public IPwmChannel this[uint channelIndex]
            {
            get
                {
                Contract.Requires(channelIndex >= 0 && channelIndex < ChannelCount);
                throw new NotImplementedException();
                }
            }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        /// <summary>
        ///     Sets all channels to 0% duty cycle by writing the LED_FULL_OFF bit in the
        ///     ALL_LED_OFF_H register.
        /// </summary>
        public async Task SetAllChannelsOff()
            {
            throw new NotImplementedException();
            }

        public Task Reset()
            {
            throw new NotImplementedException();
            }
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        #endregion
        }
    }