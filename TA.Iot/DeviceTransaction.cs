﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: DeviceTransaction.cs Last modified: 2015-11-14@04:14 by Tim Long

namespace TA.Iot
{
    /// <summary>
    ///     Represents an atomic send-receive transaction with some addressable device.
    /// </summary>
    public class DeviceTransaction : IDeviceTransaction
    {
        protected internal DeviceTransaction(byte[] transmitBuffer, byte[] receiveBuffer)
        {
            TransmitBuffer = transmitBuffer;
            ReceiveBuffer = receiveBuffer;
        }

        /// <summary>
        ///     The byte stream to be transmitted, if any.
        /// </summary>
        /// <value>The write transaction.</value>
        public byte[] TransmitBuffer { get; protected set; }

        /// <summary>
        ///     The receive buffer which, if present, will be of the correct size to hold the expected response.
        /// </summary>
        /// <value>The read transaction.</value>
        public byte[] ReceiveBuffer { get; protected set; }
    }
}