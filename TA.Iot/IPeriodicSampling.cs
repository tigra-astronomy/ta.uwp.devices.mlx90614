// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2016 Tigra Astronomy, all rights reserved.
// 
// File: IPeriodicSampling.cs Last modified: 2016-12-19@00:43 by Tim Long

using TA.UWP;

namespace TA.Iot
    {
    /// <summary>
    ///     An object that is capable of asynchronously and periodically taking samples of some data point, such that
    ///     an output value is updated over time without further intervention.
    /// </summary>
    /// <remarks>
    ///     This behaviour is most usefully combined with the <see cref="INotifySensorValueChanged" /> behaviour,
    ///     but this is neither required nor implied.
    /// </remarks>
    public interface IPeriodicSampling
        {
        /// <summary>
        ///     Gets a value indicating whether periodic sampling is active.
        /// </summary>
        /// <value><c>true</c> if periodic sampling is active; otherwise, <c>false</c>.</value>
        bool PeriodicSamplingActive { get; }

        /// <summary>
        ///     Starts sampling the data repeatedly, at the specified interval.
        /// </summary>
        /// <param name="interval">The interval between samples.</param>
        void StartPeriodicSampling(Timeout interval);

        /// <summary>
        ///     Stops periodic sampling.
        /// </summary>
        void StopPeriodicSampling();
        }
    }