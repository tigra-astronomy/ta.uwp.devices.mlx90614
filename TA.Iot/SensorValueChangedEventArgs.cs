// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2016 Tigra Astronomy, all rights reserved.
// 
// File: SensorValueChangedEventArgs.cs Last modified: 2016-12-19@16:45 by Tim Long

using System;

namespace TA.Iot
    {
    public sealed class SensorValueChangedEventArgs : EventArgs
        {
        public SensorValueChangedEventArgs(uint channel, double value)
            {
            Channel = channel;
            Value = value;
            }

        /// <summary>
        ///     Gets the channel of a multi-channel device that generated the changed data.
        /// </summary>
        /// <value>The channel number.</value>
        public uint Channel { get; }

        /// <summary>
        ///     Gets the updated sensor value, which should be in world units.
        /// </summary>
        /// <value>The value.</value>
        public double Value { get; }
        }
    }