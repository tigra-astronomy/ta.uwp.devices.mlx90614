// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: II2cDevice.cs Last modified: 2015-11-14@04:14 by Tim Long

using System.Diagnostics.Contracts;
using Windows.Devices.I2c;

namespace TA.Iot
{
    [ContractClass(typeof(I2CDeviceContracts))]
    public interface II2CDevice
    {
        I2cConnectionSettings ConnectionSettings { get; }
        string DeviceId { get; }
        void Write(byte[] buffer);
        I2cTransferResult WritePartial(byte[] buffer);
        void Read(byte[] buffer);
        I2cTransferResult ReadPartial(byte[] buffer);
        void WriteRead(byte[] writeBuffer, byte[] readBuffer);
        I2cTransferResult WriteReadPartial(byte[] writeBuffer, byte[] readBuffer);
    }
}