﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2016 Tigra Astronomy, all rights reserved.
// 
// File: InterpolatedLookup.cs Last modified: 2016-12-19@00:30 by Tim Long

using System.Diagnostics.Contracts;
using System.Linq;

namespace TA.Iot
    {
    /// <summary>
    ///     Implements a value lookup table with linear interpolation between values.
    /// </summary>
    public sealed class InterpolatedLookup
        {
        private readonly int count;
        private readonly double highestKey;
        private readonly double[] keys;
        private readonly int lastIndex;
        private readonly double lowestKey;
        private readonly double[] values;

        public InterpolatedLookup(double[] keys, double[] values)
            {
            Contract.Requires(keys != null);
            Contract.Requires(values != null);
            Contract.Requires(keys.Length > 0);
            Contract.Requires(values.Length > 0);
            Contract.Requires(keys.Length == values.Length);
            this.keys = keys;
            this.values = values;
            count = keys.Length;
            lastIndex = count - 1;
            lowestKey = keys[0];
            highestKey = keys[lastIndex];
            }

        public double Lookup(double lookup)
            {
            // Shortcut: If table contains only 1 element, return that value.
            if (count == 1)
                return values[0];
            // Shortcut: If the lookup value is off the end of the table, return an end value.
            if (lookup <= lowestKey) return values[0];
            if (lookup >= highestKey) return values[lastIndex];
            // Shortcut: if the lookup value exactly equals a key, return that entry.
            // Note: floating point comparison with equals is risky but acceptable in this situation.
            if (keys.Any(p => p == lookup))
                return keys.First(p => p == lookup);
            /*
             * At this point, we know that the lookup value is >= first key and <= last key and not equal to any of the keys.
             * Find the key pair that surrounds the lookup value, such that k1 <= lookup < k2
             */
            var index = 0;
            while (keys[index] < lookup)
                {
                ++index;
                }
            var upperKey = keys[index];
            var upperValue = values[index];
            --index;
            var lowerKey = keys[index];
            var lowerValue = values[index];
            var keySpan = upperKey - lowerKey;
            var interpolatedDistance = (lookup - lowerKey) / keySpan; // fraction of unity
            var valueSpan = upperValue - lowerValue;
            var interpolatedValue = lowerValue + valueSpan * interpolatedDistance;
            return interpolatedValue;
            }
        }
    }