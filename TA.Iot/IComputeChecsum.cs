﻿namespace TA.Iot
{
    public interface IComputeChecsum
    {
        byte[] Checksum(params byte[] value);
    }
}