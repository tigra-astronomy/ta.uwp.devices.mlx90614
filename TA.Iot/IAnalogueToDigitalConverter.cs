// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2017 Tigra Astronomy, all rights reserved.
// 
// File: IAnalogueToDigitalConverter.cs Last modified: 2017-01-02@04:38 by Tim Long

using System.Threading.Tasks;

namespace TA.Iot
    {
    /// <summary>
    ///     A device or sensor that performs analogue to digital conversion
    /// </summary>
    public interface IAnalogueToDigitalConverter
        {
        /// <summary>
        ///     Gets a sensor channel that can be used to read values and trigger updates.
        /// </summary>
        /// <param name="channel">The channel number to be accessed.</param>
        /// <returns>
        ///     An <see cref="ISensorChannel" /> that can be used to access the channel's value stream.
        /// </returns>
        ISensorChannel this[uint channel] { get; }

        /// <summary>
        ///     Gets or sets the precision of the A to D converter, in bits; this implies the maximum value that can be obtained
        ///     from the device.
        /// </summary>
        /// <value>The precision in bits.</value>
        uint PrecisionInBits { get; }

        /// <summary>
        ///     Gets the precision of the A to D converter hardware; that is, the smallest amount by which two
        ///     consecutive readings can differ. This implementation uses a moving average so the difference between two
        ///     average values can actually be smaller than this, however the true precision is directly determined by
        ///     the bit width of the underlying device.
        /// </summary>
        /// <value>The maximum value.</value>
        double Precision { get; }

        /// <summary>
        ///     Resets and reinitializes any hardware devices and clears all readings to zero.
        /// </summary>
        /// <returns>Task.</returns>
        Task Reset();

        /// <summary>
        ///     Returns a task which, when complete, will have obtained fresh data samples for all channels.
        /// </summary>
        Task SampleAllChannels();
        }
    }