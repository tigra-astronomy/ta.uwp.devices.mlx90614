﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: DoubleExtensions.cs Last modified: 2015-11-27@20:31 by Tim Long

using System;

namespace TA.UWP.ExtensionMethods
{
    public static class DoubleExtensions
    {
        public static bool IsCloseTo(this double lhs, double rhs, double tolerance = 0.0) => Math.Abs(lhs - rhs) < tolerance;
        public static bool IsInRange(this double item, double lower, double upper) => item >= lower && item <= upper;
    }
}