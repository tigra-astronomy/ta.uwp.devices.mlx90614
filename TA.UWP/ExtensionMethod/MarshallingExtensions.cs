﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: MarshallingExtensions.cs Last modified: 2015-11-14@03:40 by Tim Long

using System;
using System.Diagnostics.Contracts;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace TA.UWP.ExtensionMethods
{
    public static class MarshallingExtensions
    {
        /// <summary>
        ///     Marshals an action to the dispatcher (UI) thread and invokes it asynchronously.
        /// </summary>
        /// <param name="action">The action to be invoked.</param>
        /// <param name="uiElement">
        ///     A user interface" element that derives from <see cref="DependencyObject" /> . This is used
        ///     to retrieve the dispatcher instance.
        /// </param>
        public static async void RunOnDispatcherThreadAsync(this DependencyObject uiElement, Action action)
        {
            Contract.Requires(uiElement != null);
            Contract.Requires(uiElement.Dispatcher != null);
            Contract.Requires(action != null);
            if (!uiElement.Dispatcher.HasThreadAccess)
                await uiElement.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => action());
            else
                action();
        }
    }
}