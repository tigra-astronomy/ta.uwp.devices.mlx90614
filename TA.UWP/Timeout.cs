// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2016 Tigra Astronomy, all rights reserved.
// 
// File: Timeout.cs Last modified: 2016-12-25@16:10 by Tim Long

using System;

namespace TA.UWP
    {
    /// <summary>
    ///     Encapsulates the concept of a time-out (or time interval) which builds upon
    ///     <see cref="System.TimeSpan" />. Implicit conversions are provided for use when ticks or
    ///     milliseconds are needed. For example, some timers take a TimeSpan, some take
    ///     milliseconds and still others take a number of system ticks. A Timeout can be
    ///     constructed from or cast to any of those types. Instances of this type are immutable.
    /// </summary>
    public struct Timeout
        {
        /// <summary>
        ///     Gets the timeout as a <see cref="TimeSpan" />.
        /// </summary>
        public TimeSpan TimeSpan { get; }

        /// <summary>
        ///     Gets the timeout expressed in milliseconds.
        /// </summary>
        public int Milliseconds
            {
            get { return (int) (TimeSpan.Ticks / TimeSpan.TicksPerMillisecond); }
            }

        /// <summary>
        ///     Gets the timeout expressed in system clock ticks.
        /// </summary>
        public long Ticks
            {
            get { return TimeSpan.Ticks; }
            }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Timeout" /> class froma a <see cref="TimeSpan" />.
        /// </summary>
        /// <param name="duration">The timeout duration.</param>
        private Timeout(TimeSpan duration) : this()
            {
            TimeSpan = duration;
            }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Timeout" /> class from the specified number of milliseconds.
        /// </summary>
        /// <param name="milliseconds">The timeout duration, in milliseconds.</param>
        private Timeout(int milliseconds) : this()
            {
            TimeSpan = TimeSpan.FromTicks(milliseconds * TimeSpan.TicksPerMillisecond);
            }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Timeout" /> class from a number of clock ticks.
        /// </summary>
        /// <param name="ticks">The timeout duration in system clock ticks.</param>
        private Timeout(long ticks) : this()
            {
            TimeSpan = TimeSpan.FromTicks(ticks);
            }

        /// <summary>
        ///     Gets a <see cref="Timeout" /> set to the specified number of milliseconds.
        /// </summary>
        /// <param name="milliseconds">The timeout interval, in whole milliseconds.</param>
        public static Timeout FromMilliseconds(int milliseconds)
            {
            return new Timeout(milliseconds);
            }

        /// <summary>
        ///     Gets a <see cref="Timeout" /> set to the specified number of system clock ticks.
        /// </summary>
        /// <param name="ticks">The ticks.</param>
        /// <returns>Timeout.</returns>
        public static Timeout FromTicks(long ticks)
            {
            return new Timeout(ticks);
            }

        /// <summary>
        ///     Gets a <see cref="Timeout" /> set to the specified interval.
        /// </summary>
        /// <param name="span">The timeout duration, as a <see cref="TimeSpan" />.</param>
        /// <returns>Timeout.</returns>
        public static Timeout FromTimeSpan(TimeSpan span)
            {
            return new Timeout(span);
            }

        /// <summary>
        ///     Gets a <see cref="Timeout" /> set to the specified number of whole seconds.
        /// </summary>
        /// <param name="seconds">The timeout interval, in seconds.</param>
        /// <returns>Timeout.</returns>
        public static Timeout FromSeconds(int seconds)
            {
            return FromMilliseconds(1000 * seconds);
            }

        /// <summary>
        ///     Gets a <see cref="Timeout" /> set to the specified number of fractional seconds.
        /// </summary>
        /// <param name="seconds">The timeout period, in seconds.</param>
        /// <returns>Timeout.</returns>
        public static Timeout FromSeconds(double seconds)
            {
            return FromTicks((long) (TimeSpan.TicksPerSecond * seconds));
            }


        /// <summary>
        ///     Performs an implicit conversion from <see cref="Timeout" /> to <see cref="TimeSpan" />.
        /// </summary>
        /// <param name="t">The t.</param>
        /// <returns>The timeout value converted to a <see cref="TimeSpan" />.</returns>
        public static implicit operator TimeSpan(Timeout t)
            {
            return t.TimeSpan;
            }

        /// <summary>
        ///     Performs an implicit conversion from <see cref="Timeout" /> to <see cref="System.Int32" />.
        /// </summary>
        /// <param name="t">The t.</param>
        /// <returns>The timeout value converted to milliseconds.</returns>
        public static implicit operator int(Timeout t)
            {
            return t.Milliseconds;
            }

        /// <summary>
        ///     Performs an implicit conversion from <see cref="Timeout" /> to <see cref="System.Int64" />.
        /// </summary>
        /// <param name="t">The timeout instance.</param>
        /// <returns>The timeout value expressed in system clock ticks.</returns>
        public static implicit operator long(Timeout t)
            {
            return t.Ticks;
            }

        /// <summary>
        ///     Implements the + operator.
        /// </summary>
        /// <param name="lhs">The LHS.</param>
        /// <param name="rhs">The RHS.</param>
        /// <returns>The result of the operator.</returns>
        public static Timeout operator +(Timeout lhs, Timeout rhs)
            {
            return lhs.Plus(rhs);
            }

        /// <summary>
        ///     Returns a new instance with the combined interval of this instance and another instance.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>A new Timeout instance containing the combined time span.</returns>
        public Timeout Plus(Timeout other)
            {
            return new Timeout(TimeSpan + other.TimeSpan);
            }

        /// <summary>
        ///     Gets a <see cref="Timeout" /> structure initialized to zero.
        /// </summary>
        /// <value>An instance of <see cref="Timeout" /> initialized to zero ticks.</value>
        public static Timeout Zero => FromTicks(0);

        public override string ToString()
            {
            return TimeSpan.ToString();
            }
        }
    }