﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2016 Tigra Astronomy, all rights reserved.
// 
// File: MLX90614Thermometer.cs Last modified: 2016-09-25@00:50 by Tim Long

using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Windows.Devices.I2c;
using TA.Iot;
using TA.UWP;
using TA.UWP.ExtensionMethods;

namespace TA.Devices
{
    /// <summary>
    ///     A device driver for the Melexis MLX90614 contactless temperature sensor. Models a device with 3 channels
    ///     (ambient, field 1 and field 2) although not all device variants have all of the channels.
    /// </summary>
    public sealed class MLX90614Thermometer : ITemperatureSensor, INotifySensorValueChanged, IPeriodicSampling
    {
        private const byte ControlRegisterReset = 0x04;
        private const int ControlRegisterAutoIncrementBit = 2;
        public const int NumberOfChannels = 3;
        public const int DefaultSlaveAddress = 0x5a;
        private static readonly Octet ControlRegisterChannelMask = 0xFC;
        private readonly double[] ChannelInstantaneousValues;
        private readonly MovingAverage[] ChannelMovingAverage;
        private readonly double[] ChannelReferenceValues;
        private readonly SynchronizationContext syncContext;
        private readonly II2CDevice target;
        private Maybe<Timer> sampleTimer = Maybe<Timer>.Empty;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MLX90614Thermometer" /> class.
        /// </summary>
        /// <param name="target">The pre-configured I2C device to be used.</param>
        /// <param name="movingAverageWindow">The moving average window. Optional; default = 2</param>
        public MLX90614Thermometer(II2CDevice target, uint movingAverageWindow = 2)
        {
            Contract.Requires(target != null);
            //Contract.Requires(
            //    target.ConnectionSettings.BusSpeed == I2cBusSpeed.StandardMode,
            //    "Bus mode not supported. MLX90614 devices support StandardMode bus speed (up to 100KHz).");
            Contract.Requires(movingAverageWindow >= 2);
            this.target = target;

            syncContext = SynchronizationContext.Current; // Capture the creating thread's synchronization context.
            ChannelMovingAverage = new MovingAverage[NumberOfChannels];
            for (var i = 0; i < NumberOfChannels; i++)
                ChannelMovingAverage[i] = new MovingAverage((int)movingAverageWindow);
            ChannelInstantaneousValues = new double[NumberOfChannels];
            ChannelReferenceValues = new double[NumberOfChannels];
            ValueChangedThreshold = Precision;
        }

        /// <summary>
        ///     Gets or sets the offset, which is added to readings after they have been obtained and scaled. The default
        ///     offset is 0.0 to yield native readings in Kelvin. To obtain readings in Celsius, set an offset of -273.15; to
        ///     obtain Farenheit use an offset of -459.67 and also set a <see cref="Scale" /> of 1.8.
        /// </summary>
        /// <value>The offset.</value>
        public double Offset { get; set; } = 0.0;


        /// <summary>
        ///     Gets the precision of the thermometer in Kelvin; that is, the smallest amount by which two
        ///     consecutive readings can differ. This implementation uses a moving average so the difference between two
        ///     average values can actually be smaller than this, however the true precision is directly determined by
        ///     the bit width of the underlying device.
        /// </summary>
        /// <value>The maximum value.</value>
        public double Precision => 0.02;

        public event EventHandler<SensorValueChangedEventArgs> ValueChanged;

        /// <summary>
        ///     Gets or sets the amount that a channel's value must change by in order to trigger the <see cref="ValueChanged" />
        ///     event.
        ///     The default value for this property is determined by the device's precision and is implementation specific.
        /// </summary>
        /// <value>The threshold.</value>
        public double ValueChangedThreshold { get; set; }

        /// <summary>
        ///     Starts automatic periodic sampling. <see cref="ValueChanged" /> events will be raised whenever a channel's value
        ///     changes.
        /// </summary>
        /// <param name="interval">The interval.</param>
        public void StartPeriodicSampling(UWP.Timeout interval)
        {
            StopPeriodicSampling(); // Stops and disposes any previous timer
            var timer = new Timer(HandleSampleTimerTick, null, interval.TimeSpan, interval.TimeSpan);
            sampleTimer = new Maybe<Timer>(timer);
        }

        /// <summary>
        ///     Stops automatic periodic sampling.
        /// </summary>
        public void StopPeriodicSampling()
        {
            if (sampleTimer.Any())
            {
                sampleTimer.Single().Dispose();
                sampleTimer = Maybe<Timer>.Empty;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether periodic sampling is active.
        /// </summary>
        /// <value><c>true</c> if periodic sampling is active; otherwise, <c>false</c>.</value>
        public bool PeriodicSamplingActive => sampleTimer.Any();

        /// <summary>
        ///     Gets or sets the scale factor that is applied to readings as they are obtained.
        ///     The default scale for a newly created instance is 1.0, which yields readings directly in Kelvin or, with an
        ///     appropriate <see cref="Offset" />, in Celsius.
        ///     To obtain Farenheit, use a scale of 1.8 and an <see cref="Offset" /> of -459.67.
        /// </summary>
        /// <value>
        ///     The scale -
        /// </value>
        public double Scale { get; set; } = 1.0;

        /// <summary>
        ///     Gets or clears the moving average value of the specified channel.
        /// </summary>
        /// <param name="channel">The channel to be read.</param>
        /// <returns>The moving average value of the channel taken over the population size specified in the constructor.</returns>
        public double this[uint channel]
        {
            get
            {
                var item = ChannelMovingAverage[channel].Average;
                return item;
            }
        }

        /// <summary>
        ///     Resets and reinitializes any hardware devices and clears all readings to zero.
        /// </summary>
        public void Reset()
        {
            for (var i = 0; i < NumberOfChannels; i++)
            {
                ChannelInstantaneousValues[i] = 0;
                ChannelMovingAverage[i].Clear();
            }
        }

        /// <summary>
        ///     sample all channels as an asynchronous operation.
        /// </summary>
        public async Task SampleAllChannelsAsync()
        {
            await Task.Run(() => SampleAllChannels());
        }

        /// <summary>
        ///     Gets I2C connection settings that are appropriate for this device.
        /// </summary>
        /// <param name="address">
        ///     The I2C slave address of the device. The MLX90614 has allows its address to be programmed in EEPROM, but
        ///     from the factory they come with a default of 0x5a. Note that the device will also respond to write
        ///     commands at address 0, which allows an alternative slave address to be programmed into the EEPROM
        ///     without knowing the previously programmed address.
        /// </param>
        /// <returns>Suitably configured I2cConnectionSettings.</returns>
        public static I2cConnectionSettings ConnectionSettings(int address = DefaultSlaveAddress)
        {
            return new I2cConnectionSettings(address)
            {
                BusSpeed = I2cBusSpeed.StandardMode,
                SharingMode = I2cSharingMode.Shared
            };
        }

        private async void HandleSampleTimerTick(object ignored)
        {
            try
            {
                await SampleAllChannelsAsync();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex); // Log and ignore
            }
        }

        private void RaiseValueChanged(uint channel, double value)
        {
            var eventArgs = new SensorValueChangedEventArgs(channel, value);
            ValueChanged?.Invoke(this, eventArgs);
        }


        /// <summary>
        ///     Samples all channels and raises <see cref="ValueChanged" /> events as appropriate.
        /// </summary>
        /// <exception cref="TimeoutException"></exception>
        public void SampleAllChannels()
        {
            for (uint i = 0; i < NumberOfChannels; i++)
            {
                var builder = new SimpleTransactionBuilder().Append((byte)(0x06 + i)).ExpectResponse(3);
                var transaction = target.Transact(builder);
                if (transaction.Result.Status != I2cTransferStatus.FullTransfer)
                {
                    continue; // Just skip the sample if it times out
                    throw new TimeoutException(
                        $"I2C operation failed with status {transaction.Result.Status} after transferring {transaction.Result.BytesTransferred} bytes");
                }
                var rxBuffer = transaction.ReceiveBuffer;
                var rawADU = (rxBuffer[1] << 8) + rxBuffer[0];
                // ToDo: compute and validate checksum
                var sample = rawADU * Precision * Scale + Offset;
                ChannelInstantaneousValues[i] = sample;
                ChannelMovingAverage[i].AddSample(sample);
                var newAverageValue = ChannelMovingAverage[i].Average;
                if (Math.Abs(ChannelReferenceValues[i] - newAverageValue) > ValueChangedThreshold)
                {
                    ChannelReferenceValues[i] = newAverageValue;
                    RaiseValueChanged(i, newAverageValue);
                }
            }
        }

        public void SetEmissivity(float emissivityFactor)
        {
            var crc = new Crc8(Crc8.CCITTPolynomial);
            ushort e = (ushort)(65536.0 * emissivityFactor - 1).Clip(0.0, 65535.0);
            byte eLow = (byte)e;
            byte eHigh = (byte)(e >> 8);
            var builder = new SimpleTransactionBuilder()
                .Append(0x24)   // EEPROM access address=0x04   
                .Append(0, 0)    // Write zero to erase EEPROM cell
                .AppendChecksum(crc, new byte[] { SlaveAddressWrite });
            var result = target.Transact(builder);
            if (result.Result.Status != I2cTransferStatus.FullTransfer)
            {
                throw new TimeoutException(
                    $"Failed to clear EEPROM cell 0x04 with status {result.Result.Status} after transferring {result.Result.BytesTransferred} bytes");
            }

            builder = new SimpleTransactionBuilder()
                .Append(0x24)           // EEPROM access address=0x04   
                .Append(eHigh, eLow)    // Write the new emissivity factor
                .AppendChecksum(crc, new byte[] { SlaveAddressWrite });
            result = target.Transact(builder);
            if (result.Result.Status != I2cTransferStatus.FullTransfer)
            {
                throw new TimeoutException(
                    $"Failed to write emissivity to EEPROM cell 0x04 with status {result.Result.Status} after transferring {result.Result.BytesTransferred} bytes");
            }
        }

        private byte SlaveAddressRead => (byte)(target.ConnectionSettings.SlaveAddress << 1);
        private byte SlaveAddressWrite => (byte)(target.ConnectionSettings.SlaveAddress << 1 | 0x01);
    }
}