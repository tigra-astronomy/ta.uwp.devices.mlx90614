﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: ITemperatureSensorDataService.cs Last modified: 2015-12-27@05:47 by Tim Long

using TA.Iot;

namespace TA.MLX90614.Display.Model
{
    public interface ITemperatureSensorDataService : ITemperatureSensor, INotifySensorValueChanged, IPeriodicSampling
    {
        string DataSourceDescription { get; }
        void SetEmissivity(float emissivityFactor);
    }
}