﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2016 Tigra Astronomy, all rights reserved.
// 
// File: TemperatureSensorDataService.cs Last modified: 2016-07-19@08:53 by Tim Long

using System;
using System.Threading.Tasks;
using TA.Devices;
using TA.Iot;
using TA.UWP;

namespace TA.MLX90614.Display.Model
    {
    public class TemperatureSensorDataService : ITemperatureSensorDataService
        {
        private readonly MLX90614Thermometer sensor;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TemperatureSensorDataService" /> class.
        /// </summary>
        /// <param name="sensor">The sensor.</param>
        /// <param name="sourceDescription"></param>
        public TemperatureSensorDataService(MLX90614Thermometer sensor, string sourceDescription)
            {
            this.sensor = sensor;
            DataSourceDescription = sourceDescription;
            }

        /// <summary>
        ///     Gets or sets the offset, which is added to readings after they have been obtained and scaled. The
        ///     default offset is 0.0 to yield native readings in Kelvin. To obtain readings in Celsius, set an offset
        ///     of -273.15; to obtain Farenheit use an offset of -459.67 and also set a
        ///     <see cref="MLX90614Thermometer.Scale" /> of 1.8.
        /// </summary>
        /// <value>The offset.</value>
        public double Offset
            {
            get { return sensor.Offset; }
            set { sensor.Offset = value; }
            }

        /// <summary>
        ///     Gets the precision of the thermometer in Kelvin; that is, the smallest amount by which two
        ///     consecutive readings can differ. This implementation uses a moving average so the difference between two
        ///     average values can actually be smaller than this, however the true precision is directly determined by
        ///     the bit width of the underlying device.
        /// </summary>
        /// <value>The maximum value.</value>
        public double Precision => sensor.Precision;

        public event EventHandler<SensorValueChangedEventArgs> ValueChanged
            {
            add { sensor.ValueChanged += value; }
            remove { sensor.ValueChanged -= value; }
            }

        /// <summary>
        ///     Gets or sets the amount that a channel's value must change by in order to trigger the
        ///     <see cref="MLX90614Thermometer.ValueChanged" />
        ///     event.
        ///     The default value for this property is determined by the device's precision and is implementation specific.
        /// </summary>
        /// <value>The threshold.</value>
        public double ValueChangedThreshold
            {
            get { return sensor.ValueChangedThreshold; }
            set { sensor.ValueChangedThreshold = value; }
            }

        /// <summary>
        ///     Starts automatic periodic sampling. <see cref="MLX90614Thermometer.ValueChanged" /> events will be raised whenever
        ///     a channel's value
        ///     changes.
        /// </summary>
        /// <param name="interval">The interval.</param>
        public void StartPeriodicSampling(Timeout interval)
            {
            sensor.StartPeriodicSampling(interval);
            }

        /// <summary>
        ///     Stops automatic periodic sampling.
        /// </summary>
        public void StopPeriodicSampling()
            {
            sensor.StopPeriodicSampling();
            }

        /// <summary>
        ///     Gets a value indicating whether periodic sampling is active.
        /// </summary>
        /// <value><c>true</c> if periodic sampling is active; otherwise, <c>false</c>.</value>
        public bool PeriodicSamplingActive => sensor.PeriodicSamplingActive;

        /// <summary>
        ///     Gets or sets the scale factor that is applied to readings as they are obtained.
        ///     The default scale for a newly created instance is 1.0, which yields readings directly in Kelvin or, with an
        ///     appropriate <see cref="MLX90614Thermometer.Offset" />, in Celsius.
        ///     To obtain Farenheit, use a scale of 1.8 and an <see cref="MLX90614Thermometer.Offset" /> of -459.67.
        /// </summary>
        /// <value>
        ///     The scale -
        /// </value>
        public double Scale
            {
            get { return sensor.Scale; }
            set { sensor.Scale = value; }
            }

        /// <summary>
        ///     Gets or clears the moving average value of the specified channel.
        /// </summary>
        /// <param name="channel">The channel to be read.</param>
        /// <returns>The moving average value of the channel taken over the population size specified in the constructor.</returns>
        public double this[uint channel] => sensor[channel];

        /// <summary>
        ///     Resets and reinitializes any hardware devices and clears all readings to zero.
        /// </summary>
        /// <returns>Task.</returns>
        public void Reset() => sensor.Reset();

        /// <summary>
        ///     sample all channels as an asynchronous operation.
        /// </summary>
        public Task SampleAllChannelsAsync() => sensor.SampleAllChannelsAsync();

        #region Implementation of ITemperatureSensorDataService
        public string DataSourceDescription { get; }
        #endregion

        /// <summary>
        ///     Samples all channels and raises <see cref="MLX90614Thermometer.ValueChanged" /> events as appropriate.
        /// </summary>
        public void SampleAllChannels()
            {
            sensor.SampleAllChannels();
            }

        public void SetEmissivity(float emissivityFactor) => sensor.SetEmissivity(emissivityFactor);
    }
    }