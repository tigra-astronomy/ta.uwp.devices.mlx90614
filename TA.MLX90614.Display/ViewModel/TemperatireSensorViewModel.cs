﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: TemperatireSensorViewModel.cs Last modified: 2015-12-24@01:44 by Tim Long

using System.Threading.Tasks;
using Windows.UI.Xaml;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Threading;
using GalaSoft.MvvmLight.Views;
using TA.UWP;
using TA.Iot;
using TA.MLX90614.Display.Model;
using System;

namespace TA.MLX90614.Display.ViewModel
{
    public class TemperatureSensorViewModel : ViewModelBase
    {
        const double AbsoluteZero = -273.15;
        readonly ITemperatureSensorDataService dataService;
        readonly INavigationService navigationService;

        public TemperatureSensorViewModel(
            ITemperatureSensorDataService dataService, INavigationService navigationService)
        {
            this.dataService = dataService;
            this.navigationService = navigationService;
            Initialize();
        }

        void Initialize()
        {
            //await TakeSensorSample();
            dataService.Reset();
        }

        RelayCommand startAutoSamplingCommand;
        RelayCommand stopAutoSamplingCommand;
        RelayCommand takeSampleCommand;
        RelayCommand exitCommand;
        double ambientTemperature;
        double channel1Temperature;
        double channel2Temperature;
        private float emissivity = 1.0f;
        private RelayCommand setEmissivityCommand;

        public double Ambient
        {
            get { return ambientTemperature; }
            private set
            {
                ambientTemperature = value;
                DispatcherHelper.CheckBeginInvokeOnUI(() => RaisePropertyChanged());
            }
        }

        public double Channel1
        {
            get { return channel1Temperature; }
            private set
            {
                channel1Temperature = value;
                DispatcherHelper.CheckBeginInvokeOnUI(() => RaisePropertyChanged());
            }
        }

        public double Channel2
        {
            get { return channel2Temperature; }
            private set
            {
                channel2Temperature = value;
                DispatcherHelper.CheckBeginInvokeOnUI(() => RaisePropertyChanged());
            }
        }

        public float Emissivity
        {
            get { return emissivity; }
            set
            {
                emissivity = value;
                RaisePropertyChanged();
            }
        }

        public string DataSourceDescription => dataService.DataSourceDescription;

        /*
         * Command Properties
         */

        public RelayCommand StartAutoSamplingCommand
            =>
                startAutoSamplingCommand
                ?? (startAutoSamplingCommand = new RelayCommand(StartAutoSampling, CanStartAutoSampling));

        public RelayCommand StopAutoSamplingCommand
            =>
                stopAutoSamplingCommand
                ?? (stopAutoSamplingCommand = new RelayCommand(StopAutoSampling, CanStopAutoSampling));

        public RelayCommand TakeSampleCommand
            =>
                takeSampleCommand
                ?? (takeSampleCommand = new RelayCommand(async () => await TakeSensorSample(), CanTakeManualSample));

        public RelayCommand ExitCommand => exitCommand ?? (exitCommand = new RelayCommand(Exit, CanExit));

        public RelayCommand SetEmissivityCommand => setEmissivityCommand ?? (setEmissivityCommand = new RelayCommand(SetEmissivity));

        private void SetEmissivity()
        {
            dataService.SetEmissivity(emissivity);
        }

        void Exit() { Application.Current.Exit(); }

        bool CanStopAutoSampling() => dataService.PeriodicSamplingActive;

        void StopAutoSampling()
        {
            dataService.StopPeriodicSampling();
            dataService.ValueChanged -= HandleSensorValueChanged;
            UpdateUiState();
        }


        /// <summary>
        ///     Updates the state of the UI by raising *CanExecuteChanged events.
        /// </summary>
        void UpdateUiState()
        {
            StartAutoSamplingCommand.RaiseCanExecuteChanged();
            StopAutoSamplingCommand.RaiseCanExecuteChanged();
            TakeSampleCommand.RaiseCanExecuteChanged();
            ExitCommand.RaiseCanExecuteChanged();
        }

        bool CanStartAutoSampling() => !dataService.PeriodicSamplingActive;
        bool CanExit() => true;
        bool CanTakeManualSample() => !dataService.PeriodicSamplingActive;

        void StartAutoSampling()
        {
            dataService.ValueChanged += HandleSensorValueChanged;
            dataService.StartPeriodicSampling(Timeout.FromMilliseconds(200));
            UpdateUiState();
        }

        void HandleSensorValueChanged(object sender, SensorValueChangedEventArgs e)
        {
            switch (e.Channel)
            {
                case 0:
                    Ambient = e.Value;
                    break;
                case 1:
                    Channel1 = e.Value;
                    break;
                case 2:
                    Channel2 = e.Value;
                    break;
            }
        }

        async Task TakeSensorSample()
        {
            await dataService.SampleAllChannelsAsync();
            Ambient = dataService[0];
            Channel1 = dataService[1];
            Channel2 = dataService[2];
        }
    }
}